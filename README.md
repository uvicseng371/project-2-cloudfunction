# Project 2 Cloud Function
### Summary
This is a Azure CloudFunction that updates the forest fire observations for [ForestFyre](https://www.forestfyre.xyz) every hour. It should be treated as a black box when evaluating the system.

See [Project 2](https://gitlab.com/uvicseng371/project-2) for a description of the entire project.

### Description
This function can be treated as a black box when evaluating the ForestFyre application, as it simply generates data for the app and inserts it into the database for consumption. In a real system, a different cloud function which triggers on new satelite images and runs a geospatial image analysis algorithm. Our application (ForestFyre) will work as-is with any data-source as long as it inserts correctly formatted data in to the database. This function is simply generating random demo data, as we do not have the resources or background to accurately analyze real imagery. 

This cloud function is triggered every hour to take new observations. Currently it runs at xx:51 for any hour xx. 

The latest observations for existing fires are retrieved from the database, and updated observations are created by simulating the fire's evolution. The updated observations are written back to the database for consumption by the ForestFyre application. New fires are discovered by random chance.

### Evolution Details
Fires are updated at each tick using either *growth* or *decay*. Whether fires are growing or decaying is based on functions of the fire's age and radius. In general, young fires usually grow and old fires usually decay, but the actual behaviour of each fire is determined randomly each tick. When fires are old and small enough, they have a random chance of being extinguished each tick.

New fires are generated at random coordinates (bounded to a rectangle over the western Canadian provinces and north-western US states). New fires are guaranteed to grow for at least 12 hours before being able to decay.
