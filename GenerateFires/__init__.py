import logging
import math
import os
import random
import time
from datetime import datetime, timezone

import azure.functions as func
import psycopg2
from psycopg2.extras import RealDictCursor


logging.basicConfig(level=logging.DEBUG)
TIME_STEP = int(os.environ.get("APP_TIME_STEP", 0))
CURRENT_TIME = int(os.environ.get("APP_CURRENT_TIME", time.time()))

with open("GenerateFires/notes.txt") as f:
    NOTES = [line.rstrip("\n") for line in f]


def main(mytimer: func.TimerRequest) -> None:
    logging.info(
        "Python timer trigger function ran at %s",
        datetime.utcnow().replace(tzinfo=timezone.utc).isoformat(),
    )
    process_fires()


def process_fires():
    # Connect to the database
    conn = psycopg2.connect(
        database=os.environ["APP_DB_NAME"],
        host=os.environ["APP_DB_HOST"],
        user=os.environ["APP_DB_USER"],
        password=os.environ["APP_DB_PASS"],
    )

    # Retrieve latest fires to evolve
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(
        """
        select fires.*, discovered from fires join (
            select identifier, max("timestamp") as "timestamp" from fires
            group by identifier
        ) "ids" on fires.identifier = ids.identifier and fires."timestamp" = ids."timestamp"
        join (
            select identifier, min("timestamp") as discovered from fires
            group by identifier
        ) discovered on fires.identifier = discovered.identifier
        where radius != 0
        """
    )
    result = cur.fetchall()

    # Generate new fires based on old fires
    fires = [evolve_step(x) for x in result]
    fires = [x for x in result if x is not None]
    data = [
        (
            x["identifier"],
            x["latitude"],
            x["longitude"],
            x["radius"],
            CURRENT_TIME if TIME_STEP == 0 else int(x["timestamp"]) + TIME_STEP,
            x["note"],
        )
        for x in fires
    ]

    no_fires = len(data) == 0
    if not no_fires:
        # Insert the updated fires to the DB
        insert_query = "insert into fires (identifier, latitude, longitude, radius, timestamp, note) values %s"
        psycopg2.extras.execute_values(
            cur, insert_query, data, template=None, page_size=100
        )

    # Generate a new fire
    new_fire = detect_fire(force=no_fires)
    if new_fire is not None:
        # Find the next available fire identifier
        cur.execute("""select max(identifier) from fires""")
        row = cur.fetchone()
        if row["max"] is None:
            identifier = 1
        else:
            identifier = int(row["max"]) + 1
        logging.debug(f"Assigning new fire identifier: {identifier}")
        new_fire["identifier"] = identifier

        # Prepare data
        logging.info(f"Discovered new fire: {new_fire}")
        data = [
            (
                identifier,
                new_fire["latitude"],
                new_fire["longitude"],
                new_fire["radius"],
                CURRENT_TIME,
                new_fire["note"],
            )
        ]
        insert_query = "insert into fires (identifier, latitude, longitude, radius, timestamp, note) values %s"
        psycopg2.extras.execute_values(
            cur, insert_query, data, template=None, page_size=100
        )
    else:
        logging.debug("No new fire discovered")

    # Commit the changes
    conn.commit()


def evolve_step(fire):
    age = CURRENT_TIME - fire["discovered"]
    age = age / 3600

    # plot these to see how decay and growth fluctuate with time, with decay slowly overtaking
    growth_potential = 25 * math.sin(
        random.uniform(0.8, 1.1) * (age / 60) + random.random()
    )
    decay_limit = (
        20 * math.sin(age / 58) + math.sqrt(age / random.uniform(0.1, 0.5)) - 30
    )
    logging.debug(
        f"fire {fire['identifier']}: age (growth/decay): {age} ({growth_potential} {decay_limit})"
    )
    if age < 12 or growth_potential > decay_limit:
        return evolve_grow(fire)
    else:
        return evolve_decay(fire)


def evolve_grow(fire):
    logging.info(f"growing fire {fire['identifier']}")
    max_growth = fire["radius"] * 0.05  # 5% of current size
    fire["radius"] += max_growth * (random.random() ** 2)
    return fire


def evolve_decay(fire):
    if fire["radius"] < 2000:
        if evolve_extinguish(fire):
            logging.info(f"extinguished fire {fire['identifier']}")
            fire["radius"] = 0
            return fire
    logging.info(f"decaying fire {fire['identifier']}")
    max_decay = fire["radius"] * 0.10  # 10% of current size
    fire["radius"] -= max_decay * (random.random() ** 2)
    return fire


def evolve_extinguish(fire):
    logging.debug(f"considering extinguishing fire {fire['identifier']}")
    if fire["radius"] < 250:
        logging.debug(f"extinguishing fire {fire['identifier']} (due to size)")
        return True
    if random.random() < 0.2:
        logging.debug(f"extinguishing fire {fire['identifier']} (random chance)")
        return True
    return False


def detect_fire(force=False):
    if force:
        logging.debug("Forcing new fire creation")
    if force or random.random() < 0.025:
        return generate_fire()
    return None


def generate_fire():
    return {
        "latitude": random.uniform(42, 60),
        "longitude": random.uniform(-124, -100),
        "radius": random.uniform(250, 1000),
        "note": random.choice(NOTES),
    }


if __name__ == "__main__":
    process_fires()
